#!/bin/bash
# This script can download a bunch of files and merge them into one pdf
# author: caminsha

# define the separate files (just write the url inside the parentheses)
PREFACE=""
BIBLIOGRAPHY=""

download_files() {
    wget $PREFACE -O "OUTPUTFILE_preface"

    for chapter in {1..29}; do
        if [[ chapter -le 9 ]]; then
            wget "" -O "OUTPUTFILE"
        else
            wget "" -O "OUTPUTFILE"
        fi
    done
    wget $BIBLIOGRAPHY -O "OUTPUTFILE_bib"
}

merge_files() {
    CHAPTERS=$(ls | grep -Ev ".*(bib|pref|download).*)
    pdfunite preface.pdf ${CHAPTERS} bibliography.pdf finalPDF.pdf
}

print_help() {
    echo "This script downloads a bunch of files and merges them"
    echo ""
    echo "Usage: $0 [ARGUMENTS]"
    echo 
    echo "Arguments:"
    echo -e "-h|help\t\t shows this help dialog"
    echo -e "-d|download\t Downloads the specified files"
    echo -e "-m|merge\t merges the PDF files into one document"
    echo -e "-a|all\t\t Combine download and  merge"
}

ACTION=$1
case "$ACTION" in 
    -h|help)
        print_help
        ;;
    -d|download)
        download_files
        ;;
    -m|merge)
        merge_files
        ;;
    -a|all)
        download_files
        merge_files
        ;;
    *)
        print_help
        ;;
esac
