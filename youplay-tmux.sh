#!/bin/bash

# This script is based on the youplay.py file.
# The script for youplay.py can be found here:
# When using this script it is possible to listen to youtube music in the background.

# In this script I assume that youtube-dl is installed with pip3.
# The pip-binaries are installed into a virtualenv in `~/videos`

# ========================================================================== #
# Variables
# ========================================================================== #
SESSION_NAME="music"

COLOR_RESET=$(tput sgr0)
GREEN='\033[0;32m' 
LIGHT_GREEN='\033[1;32m' 


# ========================================================================== #
# Functions
# ========================================================================== #
tmux_attach_session(){
    tmux attach-session -t "${SESSION_NAME}"
}

start_youplay(){
    SESSION_EXISTS=$(tmux has-session -t "${SESSION_NAME}" > /dev/null 2>&1; echo $?)
    if [[ $SESSION_EXISTS -eq 0 ]]; then
        tmux_attach_session
    else
        tmux new-session -d -s ${SESSION_NAME} 
        tmux send-keys -t "${SESSION_NAME}" "cd ~/videos " C-m
        tmux send-keys -t "${SESSION_NAME}" "source venv/bin/activate" C-m
        tmux send-keys -t "${SESSION_NAME}" "python3 youplay.py" C-m
        tmux_attach_session
    fi
}


quit_youplay(){
    echo "Tmux Session was closed and program was quit."
    tmux send-keys -t "${SESSION_NAME}" C-c
    tmux send-keys -t "${SESSION_NAME}" deactivate C-c
    tmux kill-session -t "${SESSION_NAME}"
}

print_help(){
    echo ""
    echo -e "${LIGHT_GREEN}=================================================${COLOR_RESET}"
    echo "This script is used with tmux."
    echo "It opens a tmux session to search for a song."
    echo -e "${GREEN}-------------------------------------------------${COLOR_RESET}"
    echo "Arguments:"
    echo ""
    echo -e "-h|help|--help\t\t Prints this help"
    echo -e "-q|quit|--quit\t\t Closes the tmux session"
    echo -e "All other\t\t Open tmux session"
    echo -e "${LIGHT_GREEN}=================================================${COLOR_RESET}"
    echo ""
}

# ========================================================================== #
# Entry Point
# ========================================================================== #

ARG=$1

case $ARG in 
    -h|help|--help)
        print_help
        ;;
    -q|quit|--quit)
        quit_youplay
        ;;
    *)
        start_youplay
        ;;
esac
