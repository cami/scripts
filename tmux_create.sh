#!/bin/bash

COL_RESET=$(tput sgr0)
F_RED=$(tput setaf 1)
F_GREEN=$(tput setaf 2)

verify_creation() {
    if [[ $? -eq 0 ]]; then
        echo "${F_GREEN}Session ${session} created successfully${COL_RESET}"
    else
        echo "${F_RED}Session ${session} failed to create${COL_RESET}"
    fi
}

cd "$HOME" || exit

session="srv"
tmux new-session -d -s $session
verify_creation

cd "$HOME"/repos/GnuLinux-Beitraege || exit

session="gln"
tmux new-session -d -s $session
verify_creation


cd "$HOME"/videos || exit
session="tv"
tmux new-session -d -s $session
tmux send-keys -t $session 'source ./venv/bin/activate' C-m
verify_creation
