#!/bin/bash

readonly STARTTIME_H=$1
readonly STARTTIME_M=$2


printStartEndLine (){
    echo "====================="
}

printPauseLine (){
    echo "---------------------"
}

printHelp (){
    echo "Usage: "
}

concatTime (){
    echo "$1:$2"
}

printPtStart (){
    echo -e "ÖV-Start\t"
}

printPtEnd (){
    echo -e "ÖV-End\t"
}

printGameStart (){
    hours=${STARTTIME_H}
    mins=${STARTTIME_M}
    time=$(concatTime "$hours" "$mins")
    echo -e "Spielbeginn\t${time}"
}

printGameEnd (){
    echo -e "Spielende\t"
}

printStadiumOpening (){
    echo -e "Stadionöffnung\t"
}

printPtStart
printStartEndLine
printStadiumOpening
printGameStart
printPauseLine
printGameEnd
printStartEndLine

