#!/bin/bash

# Original author: arth2o
# Original Link: https://gist.github.com/arth2o/fb2bd24f132716015377562317b32878

# This script will convert NEF (raw photo files) to jpg files
# First argument is the directory where the pictures are located

DIR="$@"

for f in $DIR*.NEF
do
	name=$(echo "$f" | sed -e "s/.NEF$//g")
	exiftool -b -JpgFromRaw "$f" > "${name}.jpg"
done
